package swim.player

import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.os.IBinder
import android.widget.MediaController

class MusicController(context: Context): MediaController.MediaPlayerControl, MediaController(context){

    private lateinit var songsList: List<Song>
    lateinit var musicService: MusicService
    private var isServiceBound: Boolean = false
    var currentSongIndex = -1

    init{
        setPrevNextListeners({ musicService.setNextSong() },
            { musicService.setPreviousSong() })
        setAnchorView((context as MainActivity).findViewById(R.id.main_layout))
        setMediaPlayer(this)
        isEnabled = true

    }

    constructor(context: Context, songs: List<Song>) : this(context) {
        this.songsList = songs
    }

    val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            connectService((service as MusicService.MusicBinder).getService(), songsList )
            musicService.musicController = this@MusicController
        }

        override fun onServiceDisconnected(name: ComponentName) {
            disconnectService()
        }
    }

    fun disconnectService() {
        isServiceBound = false
    }

    fun connectService(musicService: MusicService, songs: List<Song>) {
        this.musicService = musicService
        musicService.songsList = songs
        isServiceBound = true
        show()
    }

    fun songChosen(index: Int) {
        musicService.setSong(index)
        musicService.prepareSong()
    }

    override fun isPlaying(): Boolean {
        return if (isServiceBound) musicService.isPlaying()
        else { false }
    }

    override fun canSeekForward(): Boolean {
        return true
    }

    override fun getDuration(): Int {
        return if (isServiceBound && musicService.isPlaying()) musicService.getDuration()
        else { 0 }
    }

    override fun pause() {
        musicService.pauseMusic()
    }

    override fun hide() {
    }

    override fun getBufferPercentage(): Int {
        return if (isServiceBound && musicService.isPlaying()) musicService.getDuration()
        else { 0 }
    }

    override fun seekTo(pos: Int) {
        musicService.seek(pos)
    }

    override fun getCurrentPosition(): Int {
        return if (isServiceBound && musicService.isPlaying()) musicService.getPosition()
        else { 0 }
    }

    override fun canSeekBackward(): Boolean {
        return true
    }

    override fun start() {
        musicService.startMusic()
    }

    override fun getAudioSessionId(): Int {
        return MEDIA_PLAYER_SESSION_KEY
    }

    override fun canPause(): Boolean {
        return true
    }

    companion object {
        private const val MEDIA_PLAYER_SESSION_KEY = 777
    }

}