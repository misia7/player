package swim.player

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class PlaylistAdapter(private val songs : MutableList<Song>) : RecyclerView.Adapter<PlaylistAdapter.ViewHolder>() {

    inner class ViewHolder(View: View) : RecyclerView.ViewHolder(View) {

        val title: TextView = itemView.findViewById(R.id.tv_title)
        val artist: TextView = itemView.findViewById(R.id.tv_artist)
        init {
            itemView.setOnClickListener {
                (itemView.context as MainActivity).musicController.currentSongIndex = adapterPosition
                (itemView.context as MainActivity).musicController.songChosen(adapterPosition)
                notifyDataSetChanged()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val View = LayoutInflater.from(parent.context).inflate(R.layout.song_element, parent, false)
        return ViewHolder(View)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val elem: Song = songs[position]
        if((holder.itemView.context as MainActivity).musicController.currentSongIndex == position){
            holder.itemView.setBackgroundResource(R.color.songSelected)
        } else {
            holder.itemView.setBackgroundResource(R.color.songNotSelected)
        }
        holder.title.text = elem.title
        holder.artist.text = elem.artist
    }

     override fun getItemCount() = songs.size
}